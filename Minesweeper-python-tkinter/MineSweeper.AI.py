from tkinter import *
from tkinter import messagebox
from random import *
import time
import constraint
import sys

def main():
    global frontier
    global explored_set
    global lose
    global ans
    # BEGIN helper
    def zeros(g, h):
        # create zeros array with size g and h
        return [[0 for n in range(g)] for n in range(h)]

    def OpenAll():
        # open all boxes (when losing or winning)
        c = 0
        for i in w.values():
            if int(str_retrieve[c]) == 9:
                i.configure(image=Bomb, width="30", height="30")
            else:
                image(i, int(str_retrieve[c]))
            c += 1
    # END helper

    win = Tk()

    # rows,columns,number of bombs
    g = h = 9
    NumberOfBombs = 10

    # opening images
    Bomb = PhotoImage(file="bomb.gif")
    Flag2 = PhotoImage(file="flagged.gif")
    normal = PhotoImage(file="facingDown.gif")
    Blank = PhotoImage(file="0.gif")
    one = PhotoImage(file="1.gif")
    two = PhotoImage(file="2.gif")
    three = PhotoImage(file="3.gif")
    four = PhotoImage(file="4.gif")
    five = PhotoImage(file="5.gif")

    # Creating the main list
    L = zeros(g+1, h+1)
    L2 = zeros(g+1, h+1)

    actualnumber = 0

    # Inserting bombs
    for k in range(NumberOfBombs):
        a = randint(0, 9)
        b = randint(0, 9)
        if L[a][b] != 9:
            L[a][b] = 9
            actualnumber += 1


    # Counting bombs arround a square
    for x in range(g + 1):
        for y in range(h + 1):
            if L[x][y] == 9:
                if x < g:
                    if L[x + 1][y] != 9:
                        L[x + 1][y] += 1
                if x >= 1:
                    if L[x - 1][y] != 9:
                        L[x - 1][y] += 1
                if y < h:
                    if L[x][y + 1] != 9:
                        L[x][y + 1] += 1
                if y >= 1:
                    if L[x][y - 1] != 9:
                        L[x][y - 1] += 1
                if x < g and y < h:
                    if L[x + 1][y + 1] != 9:
                        L[x + 1][y + 1] += 1
                if x >= 1 and y < h:
                    if L[x - 1][y + 1] != 9:
                        L[x - 1][y + 1] += 1
                if x < g and y >= 1:
                    if L[x + 1][y - 1] != 9:
                        L[x + 1][y - 1] += 1
                if x >= 1 and y >= 1:
                    if L[x - 1][y - 1] != 9:
                        L[x - 1][y - 1] += 1


    # Changing rows with columns so that it can fit with the dictionary
    Transpose = [[L[j][i] for j in range(len(L))] for i in range(len(L[0]))]

    # Getting all numbers of the list in a string
    str_retrieve = ""
    for i in Transpose:
        z = "".join(str(f) for f in i)
        str_retrieve += z

    # opening images
    def image(x, y):
        if x in w.values():
            if y == 1:
                x.configure(image=one, width="30", height="30")
            elif y == 2:
                x.configure(image=two, width="30", height="30")
            elif y == 3:
                x.configure(image=three, width="30", height="30")
            elif y == 4:
                x.configure(image=four, width="30", height="30")
            elif y == 5:
                x.configure(image=five, width="30", height="30")
            elif y == 0:
                x.configure(image=Blank, width="30", height="30")
        else:
            if y == 1:
                x.widget.configure(image=one, width="30", height="30")
            elif y == 2:
                x.widget.configure(image=two, width="30", height="30")
            elif y == 3:
                x.widget.configure(image=three, width="30", height="30")
            elif y == 4:
                x.widget.configure(image=four, width="30", height="30")
            elif y == 5:
                x.widget.configure(image=five, width="30", height="30")
            elif y == 0:
                x.widget.configure(image=Blank, width="30", height="30")


    # Opening all zeroes around a box that have zero value
    def RemoveZeroes(i, j):
        try:
            image(w[j + 1, i], L[i][j + 1])
            image(w[j + 1, i + 1], L[i + 1][j + 1])
            image(w[j + 1, i - 1], L[i - 1][j + 1])
            image(w[j - 1, i - 1], L[i - 1][j - 1])
            image(w[j - 1, i], L[i][j - 1])
            image(w[j - 1, i + 1], L[i + 1][j - 1])
            image(w[j, i + 1], L[i + 1][j])
            image(w[j, i - 1], L[i - 1][j])

        except KeyError:
            pass

    # Adding Flags
    def Flag(event, index):
        i = index[1]
        j = index[0]
        if L2[i][j] == "Flag":
            L2[i][j] = L[i][j]
            event.widget.configure(image=normal, width="30", height="30")

        else:
            L2[i][j] = "Flag"
            event.widget.configure(image=Flag2, width="30", height="30")


    # This function works when a button is clicked
    def ClickMe(event, index):
        i = index[1]
        j = index[0]

        if L[i][j] != 9 and L[i][j] != 0 and L2[i][j] != "Flag":
            image(event, (L[i][j]))

        elif L[i][j] == 0 and L2[i][j] != "Flag":

            try:
                image(event, L[i][j])
                image(w[j + 1, i], L[i][j + 1])
                if L[i][j + 1] == 0:
                    RemoveZeroes(i, j + 1)
            except KeyError:
                pass

            try:
                image(w[j + 1, i + 1], L[i + 1][j + 1])
                if L[i + 1][j + 1] == 0:
                    RemoveZeroes(i + 1, j + 1)
            except KeyError:
                pass

            try:
                image(w[j + 1, i - 1], L[i - 1][j + 1])
                if L[i - 1][j + 1] == 0:
                    RemoveZeroes(i - 1, j + 1)
            except KeyError:
                pass

            try:
                image(w[j - 1, i - 1], L[i - 1][j - 1])
                if L[i - 1][j - 1] == 0:
                    RemoveZeroes(i - 1, j - 1)
            except KeyError:
                pass

            try:
                image(w[j - 1, i], L[i][j - 1])
                if L[i][j - 1] == 0:
                    RemoveZeroes(i, j - 1)
            except KeyError:
                pass

            try:
                image(w[j - 1, i + 1], L[i + 1][j - 1])
                if L[i + 1][j - 1] == 0:
                    RemoveZeroes(i + 1, j - 1)
            except KeyError:
                pass

            try:
                image(w[j, i + 1], L[i + 1][j])
                if L[i + 1][j] == 0:
                    RemoveZeroes(i + 1, j)
            except KeyError:
                pass

            try:
                image(w[j, i - 1], L[i - 1][j])
                if L[i - 1][j] == 0:
                    RemoveZeroes(i - 1, j)
            except KeyError:
                pass

        elif L[i][j] and L2[i][j] != "Flag":
            global lose
            if lose:
                return
            event.widget.configure(image=Bomb, width="10", height="10")
            OpenAll()
            messagebox.showinfo("Oops", "You lost")
            lose = True


    # Creating buttons
    w = {}
    # for i in L:
    # print(i)
    for i in range(g + 1):
        for j in range(h + 1):
            action = Button(win, image=normal, width="30", height="30")
            action.bind("<Button-1>", lambda event, index=(i, j): click(index))
            if sys.platform == 'darwin':
                action.bind("<Button-2>", lambda event, index=[i, j]: Flag(event, index))
            else:
                action.bind("<Button-3>", lambda event, index=[i, j]: Flag(event, index))
            action.grid(column=i, row=j)
            w[i, j] = action


    # =============SOLVER============
    """
        Rule: every tile variable in python-constraint solver,
        0 means no bomb
        1 means there is bomb inside
    """
    explored_set = set()
    frontier = set()
    lose = False
    ans = {}


    class Logger:
        def info(self, str_info):
            print('[☘ ]', str_info)

        def warning(self, str_warning):
            print('[☢ ]', str_warning)


    log = Logger()
    problem = constraint.Problem()


    def tilevar(x, y):
        return "tile,%d,%d" % (x, y)


    def unvar(str_var):
        return tuple(map(str_var.split(',')[1:]))


    def over():
        normal_count = 0
        for i in range(10):
            for j in range(10):
                if w[i, j]['image'] == str(normal) or w[i, j]['image'] == str(Flag2):
                    normal_count += 1
        return normal_count == actualnumber


    def add_constraint_to_engine(tile):
        if ans.get(tile) is not None:
            return False
        if w[tile]['image'] == str(Flag2):
            ans[tile] = 1
            tilename = tilevar(*tile)
            if tilename in problem._variables:
                problem.addConstraint(lambda arg: arg == 1, (tilename,))
            else:
                problem.addVariable(tilename, [1])
        else:
            ans[tile] = 0
            if w[tile]['image'] == str(Blank):
                for i in range(tile[0] - 1, tile[0] + 2):
                    for j in range(tile[1] - 1, tile[1] + 2):
                        tilename = tilevar(i, j)
                        if tilename in problem._variables:
                            problem.addConstraint(lambda arg: arg == 0, (tilename,))
                        else:
                            problem.addVariable(tilename, [0])
            else:
                tilename = "tile,%d,%d" % tile
                if tilename in problem._variables:
                    problem.addConstraint(lambda arg: arg == 0, (tilename,))
                else:
                    problem.addVariable(tilename, [0])
                args = []
                for i in range(tile[0] - 1, tile[0] + 2):
                    for j in range(tile[1] - 1, tile[1] + 2):
                        if (i, j) == tile:
                            continue
                        tilename = tilevar(i, j)
                        if tilename not in problem._variables:
                            problem.addVariable(tilename, [0, 1])
                        args.append(tilename)
                if w[tile]['image'] == str(one):
                    problem.addConstraint(
                        lambda a0, a1, a2, a3, a4, a5, a6, a7:
                        (a0 and not a1 and not a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and not a5 and not a6 and a7),
                        tuple(args))
                elif w[tile]['image'] == str(two):
                    problem.addConstraint(
                        lambda a0, a1, a2, a3, a4, a5, a6, a7:
                        (a0 and a1 and not a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and not a5 and a6 and a7),
                        tuple(args))
                elif w[tile]['image'] == str(three):
                    problem.addConstraint(
                        lambda a0, a1, a2, a3, a4, a5, a6, a7:
                        (a0 and a1 and a2 and not a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and not a4 and a5 and a6 and a7),
                        tuple(args))
                elif w[tile]['image'] == str(four):
                    problem.addConstraint(
                        lambda a0, a1, a2, a3, a4, a5, a6, a7:
                        (a0 and a1 and a2 and a3 and not a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and a1 and a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and not a3 and a4 and a5 and a6 and a7),
                        tuple(args))
                elif w[tile]['image'] == str(five):
                    problem.addConstraint(
                        lambda a0, a1, a2, a3, a4, a5, a6, a7:
                        (a0 and a1 and a2 and a3 and a4 and not a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and a3 and not a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and a3 and not a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and a2 and a3 and not a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and a2 and not a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and a1 and a2 and not a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and a1 and not a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (a0 and a1 and not a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (a0 and not a1 and a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and a5 and a6 and not a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and a5 and not a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and a4 and not a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and a3 and not a4 and a5 and a6 and a7) or
                        (a0 and not a1 and not a2 and not a3 and a4 and a5 and a6 and a7) or
                        (not a0 and a1 and a2 and a3 and a4 and a5 and not a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and a4 and not a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and a4 and not a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and a3 and not a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and a2 and not a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and a1 and not a2 and not a3 and a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and a5 and a6 and not a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and a5 and not a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and a4 and not a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and a3 and not a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and a2 and not a3 and a4 and a5 and a6 and a7) or
                        (not a0 and not a1 and not a2 and a3 and a4 and a5 and a6 and a7),
                        tuple(args))
        return True


    def retrieve(tile):
        event = Event()
        event.widget = w[tile[0], tile[1]]
        ClickMe(event, tile)
        win.update()


    def flag(tile):
        event = Event()
        event.widget = w[tile[0], tile[1]]
        i = tile[1]
        j = tile[0]
        L2[i][j] = "Flag"
        event.widget.configure(image=Flag2, width="30", height="30")


    def do_bfs(start_node):
        global explored_set
        global frontier
        updated = False
        frontier.add(start_node)
        while len(frontier) > 0:
            node = frontier.pop()
            if node in explored_set:
                continue
            retrieve(node)
            updated = add_constraint_to_engine(node) or updated
            explored_set.add(node)
        
            if w[node]['image'] == str(Blank):
                for i in range(node[0] - 1, node[0] + 2):
                    for j in range(node[1] - 1, node[1] + 2):
                        if i > 9 or i < 0 or j > 9 or j < 0:
                            continue
                        if (i, j) not in explored_set:
                            frontier.add((i, j))
            # log.warning(str(node))
        return updated


    def evaluate(solutions):
        # Get solution confgiration
        updated = False
        n = len(solutions)
        count = {}
        for solution in solutions:
            for str_node, v in solution.items():
                count[str_node] = count.get(str_node, 0) + v

        for str_node, v in count.items():
            node = tuple(map(int, str_node.split(',')[1:]))
            if v == n:
                flag(node)
                updated = add_constraint_to_engine(node) or updated
            i, j = node
            if i > 9 or i < 0 or j > 9 or j < 0:
                continue
            if v == 0:
                updated = do_bfs(node) or updated
        return updated


    # Add initial variable. Variable outside the arena will be cost 0
    for i in range(0, 10):
        problem.addVariable(tilevar(-1, i), [0])
        problem.addVariable(tilevar(i, -1), [0])
        problem.addVariable(tilevar(10, i), [0])
        problem.addVariable(tilevar(i, 10), [0])

    problem.addVariable(tilevar(-1, -1), [0])
    problem.addVariable(tilevar(-1, 10), [0])
    problem.addVariable(tilevar(10, -1), [0])
    problem.addVariable(tilevar(10, 10), [0])


    def click(index):
        global lose
        updated = do_bfs(index)

        while True:
            solutions = problem.getSolutions()

            if not evaluate(solutions):
                break
        if over():
            messagebox.showinfo("Yay", "You win")
            response = messagebox.askquestion ('Restart','Restart ? ')
            if response == 'yes':
                win.destroy()
                main()
        elif not lose and updated:
            messagebox.showinfo("Can't determine entailment yet..", "Click another box")
    win.mainloop()

# ==========END OF SOLVER==========

main()

